﻿using Market.Common.Brokers;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	public class OrderbookConsumer : Consumer
	{
		public override void Recived(object sender, BasicDeliverEventArgs e)
		{
			var messessage = Encoding.UTF8.GetString(e.Body.ToArray());
			Console.WriteLine(e.ConsumerTag + " - " + messessage);

		}
	}
}
