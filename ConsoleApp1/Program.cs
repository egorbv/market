﻿using Market.Common.Brokers;
using Market.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using TickTrader.BusinessObjects.EventArguments;
using TickTrader.Common.Business;
using TickTrader.Manager.Model;

namespace ConsoleApp1
{

	public enum En
	{
		First = 15,
		Second,
		Third = 54
	}

	class Program
	{
		static Publisher _publisher;

		static void Main(string[] args)
		{
			ITickTraderManagerModel manager = new TickTraderManagerModel();
			manager.PumpingUpdateSymbolTick += Manager_PumpingUpdateAllSymbolTick;
			manager.ModelDisconnected += Manager_ModelDisconnected;
			var res = manager.Connect("demonstration.tts.st.soft-fx.eu", 24, "6XZ89S9Dc3hp");
			manager.EnablePumping();
			manager.Level2Subscribe(new string[] { "ETHLTC" }, 40);

			var messageBorker = new MessageQueueBroker();
			_publisher = messageBorker.CreatePulisher(QueueType.OrderBook);
			var consumer1 = new OrderbookConsumer();
			var consumer2 = new OrderbookConsumer();
			messageBorker.RegisterConsumer(QueueType.OrderBook, consumer1);
			messageBorker.RegisterConsumer(QueueType.OrderBook, consumer2);

			Console.ReadKey();
		}

		private static void Manager_ModelDisconnected(object sender, EventArgs e)
		{
			throw new Exception("dddddddddd");
		}

		private static void Manager_PumpingUpdateAllSymbolTick(object sender, PumpingUpdateSymbolTickEventArgs e)
		{
			if(e.SymbolTick.Symbol == "ETHLTC" && e.SymbolTick.Level2 != null)
			{
				var symbolTick = e.SymbolTick;
				var orderbook = new OrderbookSymbolTick();
				orderbook.Symbol = symbolTick.Symbol;
				orderbook.Ask = symbolTick.Ask;
				orderbook.Bid = symbolTick.Bid;
				orderbook.Time = symbolTick.Time;
				var count = symbolTick.Level2.Length;
				var level2 = symbolTick.Level2;
				for(var i=0; i < count; i++)
				{
					var tick = level2[i];
					if(tick.Type == FxPriceType.Ask)
					{
						orderbook.Asks.Add(new Tick
						{
							P = tick.Price,
							V = tick.Volume
						});
					}
					else
					{
						orderbook.Bids.Add(new Tick
						{
							P = tick.Price,
							V = tick.Volume
						});

					}
				}
				_publisher.Publish(orderbook);
			}
		}
	}
}
