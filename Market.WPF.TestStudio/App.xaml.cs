﻿using Market.Common.Models;
using Market.WPF.TestStudio.Models;
using Market.WPF.TestStudio.WebSockets;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Windows;

namespace Market.WPF.TestStudio
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public readonly WebSocketCommandManager WebSocketCommandManager;

		public App()
		{
			WebSocketCommandManager = new WebSocketCommandManager();
			WebSocketCommandManager.Init();

			_orderbookSymbolTick = new BehaviorSubject<OrderbookSymbolTick>(null);
			_authificated = new BehaviorSubject<bool?>(null);
			_symbols = new BehaviorSubject<List<Symbol>>(null);

		}

		ISubject<OrderbookSymbolTick> _orderbookSymbolTick;
		ISubject<bool?> _authificated;
		ISubject<List<Symbol>> _symbols;

		public IObservable<OrderbookSymbolTick> OrderbookSymbolTick => _orderbookSymbolTick;
		public IObservable<bool?> Authificated => _authificated;

		public IObservable<List<Symbol>> Symbols => _symbols;

		public void UpdateSymbols(List<Symbol> symbols)
		{
			_symbols.OnNext(symbols);
		}

		public void UpdateAuthificated(bool success)
		{
			_authificated.OnNext(success);
		}

		public void UpdateOrderbookSymbolTikc(OrderbookSymbolTick orderbookSymbolTick)
		{
			_orderbookSymbolTick.OnNext(orderbookSymbolTick);
		}

	}

}
