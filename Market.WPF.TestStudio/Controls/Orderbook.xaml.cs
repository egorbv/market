﻿using Market.Common.Models;
using Market.WPF.TestStudio.ViewModels;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Market.WPF.TestStudio.Controls
{
	/// <summary>
	/// Interaction logic for Orderbook.xaml
	/// </summary>
	public partial class Orderbook : UserControl
	{
		public Orderbook()
		{
			InitializeComponent();
			DataContext = new OrderbookSymbolTickModel();
		}
	}
}
