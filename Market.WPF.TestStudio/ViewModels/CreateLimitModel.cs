﻿using System;
using System.Linq;
//using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Subjects;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Windows.Input;
using ReactiveUI;
using System.Reactive;
using System.Windows.Documents;

namespace Market.WPF.TestStudio.ViewModels
{
	public class CreateLimitModel : ReactiveObject
	{
		ISubject<int> _test;

		public IObservable<int> Test => _test;

		//[ObservableAsProperty]
		[Reactive]
		public int Size { get; set; }

		public ReactiveCommand<int , Unit> OkCommanmd { get; private set; }

		//public ReactiveCommand OkCommand { get; private set; }

		public CreateLimitModel()
		{
			Size = 10;

			OkCommanmd = ReactiveCommand.Create<int>((x) => {  _okCommand(x); });
			_test = new BehaviorSubject<int>(10);
			_test.Subscribe(x =>
			{
				var fr = 4;
			});
			_test.Subscribe(x =>
			{
				var fr = 4;
			});
			_test.OnNext(11);

			//Size.ssub(x => { });
			//OkCommand = ReactiveCommand.Create(() => { _okCommand(); });

		}

		Unit _okCommand(int f)
		{
			return Unit.Default;
		}
	}
}
