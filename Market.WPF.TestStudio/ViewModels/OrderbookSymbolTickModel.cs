﻿using Market.Common.Models;
using Market.WPF.TestStudio.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Text;
using System.Windows.Threading;

namespace Market.WPF.TestStudio.ViewModels
{
	public class OrderbookSymbolTickModel : ReactiveObject
	{
		Dictionary<string, Symbol> _symbols;
		OrderbookSymbolTick _lastOrderbookSymbolTick;
		string _lastSymbol;
		string _baseCurrencyFormat = "N5";
		string _quotedCurrencyFormat = "N2";


		[Reactive]
		public List<TickModel> Asks { get; set; }

		[Reactive]
		public List<TickModel> Bids { get; set; }

		[Reactive]
		public string BaseCurrency { get; set; }

		[Reactive]
		public string QuotedCurrency { get; set; }

		public OrderbookSymbolTickModel()
		{
			var app = App.Current as App;
			app.OrderbookSymbolTick.Subscribe(_update);
			app.Symbols.Subscribe(_updateSymbols);
			Asks = new List<TickModel>();
			Bids = new List<TickModel>();
		}


		void _updateSymbols(List<Symbol> symbols)
		{
			if (symbols != null)
			{
				var items = new Dictionary<string, Symbol>();
				foreach (var symbol in symbols)
				{
					items.Add(symbol.Name, symbol);
				}
				_symbols = items;
				_fillBidAndAsks();
			}
		}

		void _update(OrderbookSymbolTick orderbookSymbolTick)
		{
			if (orderbookSymbolTick != null && _symbols != null)
			{
				if(_lastSymbol != orderbookSymbolTick.Symbol)
				{
					_lastSymbol = orderbookSymbolTick.Symbol;
					var symbol = _symbols[orderbookSymbolTick.Symbol];
					BaseCurrency = symbol.BaseCurrency;
					QuotedCurrency = symbol.QuotedCurrency;
				}
				_lastOrderbookSymbolTick = orderbookSymbolTick;
				if (_symbols != null)
				{
					_fillBidAndAsks();
				}
			}
		}

		void _fillBidAndAsks()
		{
			if (_lastOrderbookSymbolTick!= null)
			{
				var asks = new List<TickModel>();
				var bids = new List<TickModel>();
				var total = 0.0d;
				for (var i = 0; i < _lastOrderbookSymbolTick.Asks.Count; i++)
				{
					var tick = _lastOrderbookSymbolTick.Asks[i];
					total += tick.V;
					asks.Add(new TickModel
					{
						Price = tick.P.ToString(_baseCurrencyFormat),
						Volume = tick.V.ToString(_quotedCurrencyFormat),
						Total = total.ToString(_quotedCurrencyFormat)
					});
				}
				total = 0.0d;
				for (var i = _lastOrderbookSymbolTick.Bids.Count - 1; i > -1; i--)
				{
					var tick = _lastOrderbookSymbolTick.Bids[i];
					total += tick.V;
					bids.Add(new TickModel
					{
						Price = tick.P.ToString(_baseCurrencyFormat),
						Volume = tick.V.ToString(_quotedCurrencyFormat),
						Total = total.ToString(_quotedCurrencyFormat)
					});
				}

				asks.Reverse();
				Asks = asks;
				Bids = bids;
			}
		}
	}

	public class TickModel
	{
		public string Price { get; set; }
		public string Volume { get; set; }

		public string Total { get; set; }
	}
}
