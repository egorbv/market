﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.WPF.TestStudio.ViewModels
{
	public class MainModel : ReactiveObject
	{
		App _app;

		[Reactive]
		public bool Authificated { get; set; }

		public MainModel()
		{
			_app = App.Current as App;
			_app.Authificated.Subscribe(_updateAuthificate);
			
		}

		void _updateAuthificate(bool? success)
		{
			if(success == true)
			{
				_app.WebSocketCommandManager.GetQuerySymbols();
			}
		}
	}
}
