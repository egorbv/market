﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.WPF.TestStudio.Models
{
	/// <summary>
	/// Модель символа
	/// </summary>
	public class Symbol
	{
		public string Name { get; set; }

		/// <summary>
		/// Коммисия за продажу базовой валюты
		/// </summary>
		public decimal BaseCurrencyCommission { get; set; }

		/// <summary>
		/// Комисия за покупку базовой валюты
		/// </summary>
		public decimal QuotedCurrencyCommission { get; set; }

		public string BaseCurrency { get; set; }

		public string QuotedCurrency { get; set; }
	}
}
