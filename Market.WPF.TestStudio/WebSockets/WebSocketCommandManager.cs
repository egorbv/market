﻿using Market.Common.Models;
using Market.WPF.TestStudio.Models;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading;
using System.Threading.Tasks;

namespace Market.WPF.TestStudio.WebSockets
{
	public class WebSocketCommandManager
	{
		ClientWebSocket _client;
		App _app;

		public void Init()
		{
			_app = App.Current as App;
			_client = new ClientWebSocket();
			_client.ConnectAsync(new Uri("wss://localhost:5001/"), new System.Threading.CancellationToken()).Wait();
			_subscribe(CancellationToken.None);
			Authorize();
		}

		public void GetQuerySymbols()
		{
			var command = new WebSocketCommand
			{
				Type = WebSocketCommandType.Query,
				Name = "query-symbols"
			};
			SendMessage(command);
		}

		public void Authorize()
		{
			var command = new WebSocketCommand
			{
				Type = WebSocketCommandType.Action,
				Name = "user-authorize",
				Args = new Login
				{
					EMail = "egorbv@mail.ru",
					Password = "1234"
				},
				UserTag = "121"

			};
			SendMessage(command);
		}

		public void SendMessage(WebSocketCommand command)
		{
			Task.Run(delegate ()
			{
				var setting = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, AllowTrailingCommas = true };
				setting.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
				var json = JsonSerializer.Serialize(command, setting);
				var message = UTF8Encoding.UTF8.GetBytes(json);
				var arraySegment = new ArraySegment<byte>(message, 0, message.Length);
				_client.SendAsync(arraySegment, WebSocketMessageType.Text, false, CancellationToken.None);
			});
		}

		void _subscribe(CancellationToken cancellationToken)
		{
			Task.Run(async delegate ()
			{
				var array = new byte[1024 * 4];
				var arraySegment = new ArraySegment<byte>(array);
				while (!cancellationToken.IsCancellationRequested)
				{
					WebSocketReceiveResult result;
					do
					{
						result = await _client.ReceiveAsync(arraySegment, cancellationToken);
					}
					while (result.EndOfMessage);

					_ff(arraySegment, result.Count);
				}
			});
		}

		void _ff(ArraySegment<byte> arraySegment, int count)
		{
			var setting = new JsonSerializerOptions() { MaxDepth = 5, PropertyNameCaseInsensitive = true, AllowTrailingCommas = true };
			var span = arraySegment.AsSpan(0, count);
			var json = UTF8Encoding.UTF8.GetString(span);
			var message = JsonSerializer.Deserialize<WebSocketMessage>(json, setting);
			switch(message.Name)
			{
				case "user-authorize":
					_updateAutificated(message);
					break;
				case "orderbook-update":
					_updateOrderBook(message);
					break;
				case "query-symbols":
					_updateSymbols(message);
					break;
				default:
					var s = message;
					break;
			}
		}

		void _updateSymbols(WebSocketMessage message)
		{
			var symbols = JsonSerializer.Deserialize<List<Symbol>>(message.Value.ToString());
			_app.UpdateSymbols(symbols);
		}

		void _updateAutificated(WebSocketMessage message)
		{
			_app.UpdateAuthificated(message.Value.ToString() == "successfull");
		}

		void _updateOrderBook(WebSocketMessage message)
		{
			var orderbookTick = JsonSerializer.Deserialize<OrderbookSymbolTick>(message.Value.ToString());
			_app.UpdateOrderbookSymbolTikc(orderbookTick);
		}

	}

	/// <summary>
	/// Модель команды полученный из веб-соккета
	/// </summary>
	public class WebSocketMessage
	{
		public string Type { get; set; }
		public string Name { get; set; }

		public object Value { get; set; }

		public string UserTag { get; set; }
	}

	//public enum WebSocketMessageType
	//{
	//	NotSet = 0,
	//	Action = 1,
	//	Query = 2
	//}
}
