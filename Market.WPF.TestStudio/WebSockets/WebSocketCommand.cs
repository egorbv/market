﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.WPF.TestStudio.WebSockets
{
	public class WebSocketCommand
	{
		/// <summary>
		/// Название комманды полученной из соккета
		/// </summary>
		public string Name { get; set; }


		/// <summary>
		/// Тип команды: action или query
		/// </summary>
		public WebSocketCommandType Type { get; set; }

		/// <summary>
		/// Данные модели для обработчика действия
		/// </summary>
		public object Args { get; set; }

		/// <summary>
		/// Пометка пользователя к команде
		/// </summary>
		public string UserTag { get; set; }
	}

	public enum WebSocketCommandType
	{
		NotSet = 0, 
		Action = 1,
		Query = 2,
	}
}
