﻿using Market.WPF.TestStudio.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Market.WPF.TestStudio.Dialogs
{
	/// <summary>
	/// Interaction logic for CreateLimit.xaml
	/// </summary>
	public partial class CreateLimit : Window
	{
		public CreateLimit()
		{
			InitializeComponent();
			DataContext = new CreateLimitModel();
		}

		private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex("[^0-9]+");
			e.Handled = regex.IsMatch(e.Text);
		}
	}
}
