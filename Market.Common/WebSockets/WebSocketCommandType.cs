﻿namespace Market.Common.WebSockets
{
	/// <summary>
	/// Тип команды web api
	/// </summary>
	public enum WebSocketCommandType
	{
		NotSet = 0,
		Action = 1,
		Query = 2,
		Notification = 3
	}
}
