﻿using Market.Common.Brokers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Market.Common.WebSockets
{
	/// <summary>
	/// Сервис получения информации о необходимым к выолнению  действия и его аргументе
	/// </summary>
	public class WebSocketCommandService : IWebSocketCommandService
	{
		/// <summary>
		/// Список зарегистрированных обработчиков действий в брокере действий
		/// </summary>
		Dictionary<string, Tuple<Type, Type>> _commands;

		/// <summary>
		/// Список зарегистрированных обработчиков действий
		/// </summary>
		Dictionary<string, Tuple<Type, Type>> _queries;

		#region constructor
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="actionBroker">Экземпляр брокера действий</param>
		/// <param name="queryBroker">Экземпляр брокера запросов</param>
		public WebSocketCommandService(IActionBroker actionBroker, IQueryBroker queryBroker)
		{
			_commands = new Dictionary<string, Tuple<Type,Type>>();
			_queries = new Dictionary<string, Tuple<Type,Type>>();

			//fills web api actions
			var actions = actionBroker.GetRegisteredWebApiActionTypes();
			foreach(var item in actions)
			{
				var actionShortName = item.Item1.ToLower();
				_commands.Add(actionShortName, new Tuple<Type, Type>(item.Item2, item.Item3));
			}

			//fills web api queries
			var queries = queryBroker.GetRegisteredWebApiQueryTypes();
			foreach(var item in queries)
			{
				var queryShortName = item.Item1.ToLower();
				_queries.Add(queryShortName, new Tuple<Type, Type>(item.Item2, item.Item3));
			}
		}
		#endregion

		#region GetAction
		/// <summary>
		/// Получить информацию о команде и ее аргументе
		/// </summary>
		/// <param name="buffer">Буфер байтового массива полученного по веб-соккету</param>
		/// <param name="count">Число заполненых байтов</param>
		/// <returns>Информация о действии и его аргументе</returns>
		public WebSocketCommand GetAction(ArraySegment<byte> buffer, int count)
		{
			var span = buffer.AsSpan(0, count);
			var msg = UTF8Encoding.UTF8.GetString(span.ToArray());
			var setting = new JsonSerializerOptions() { MaxDepth = 2, PropertyNameCaseInsensitive = true, AllowTrailingCommas = true };
			setting.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
			var webSocketCommand = JsonSerializer.Deserialize<WebSocketCommand>(span, setting);

			try
			{
				if (webSocketCommand.Type == WebSocketCommandType.Action )
				{
					var commandName = webSocketCommand.Name.ToLower();
					if (_commands.ContainsKey(commandName))
					{
						var type = _commands[commandName];
						if (webSocketCommand.Args.ValueKind != JsonValueKind.Undefined && type.Item2 != null)
						{
							var actionArgs = JsonSerializer.Deserialize(webSocketCommand.Args.GetRawText(), type.Item2, setting);
							webSocketCommand.CommandArgs = actionArgs;
						}
						webSocketCommand.Command = type.Item1;
					}
				}
				else if (webSocketCommand.Type == WebSocketCommandType.Query)
				{
					var queryName = webSocketCommand.Name.ToLower();
					if (_queries.ContainsKey(queryName))
					{
						var type = _queries[queryName];
						object actionArgs = null;
						if (webSocketCommand.Args.ValueKind != JsonValueKind.Undefined && type.Item2 != null)
						{
							actionArgs = JsonSerializer.Deserialize(webSocketCommand.Args.GetRawText(), type.Item2, setting);
							webSocketCommand.CommandArgs = actionArgs;
						}
						webSocketCommand.Command = type.Item1;
					}
				}
			}
			catch
			{
				//loger
			}
			return webSocketCommand;
		}
		#endregion
	}
}
