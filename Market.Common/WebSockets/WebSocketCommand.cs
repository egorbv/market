﻿using System;
using System.Text.Json;

namespace Market.Common.WebSockets
{
	/// <summary>
	/// Модель команды полученный из веб-соккета
	/// </summary>
	public class WebSocketCommand
	{
		/// <summary>
		/// Название комманды полученной из соккета
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Тип команды
		/// </summary>
		public Type Command { get; set; }

		/// <summary>
		/// Аргумент команды
		/// </summary>
		public object CommandArgs { get; set; }

		/// <summary>
		/// Тип команды: action или query
		/// </summary>
		public WebSocketCommandType Type { get; set; }

		/// <summary>
		/// Данные модели для обработчика действия
		/// </summary>
		public JsonElement Args { get; set; }

		/// <summary>
		/// Пометка пользователя к команде
		/// </summary>
		public string UserTag { get; set; }
	}
}
