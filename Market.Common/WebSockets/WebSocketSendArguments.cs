﻿namespace Market.Common.WebSockets
{
	/// <summary>
	/// Модель аргументов посылаемых по websocket
	/// </summary>
	public class WebSocketSendArguments
	{
		/// <summary>
		/// Тип сообщения
		/// </summary>
		public WebSocketCommandType Type { get; set; }

		/// <summary>
		/// Название сообщзения
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Данные сообщения
		/// </summary>
		public object Value { get; set; }

		/// <summary>
		/// Пользовательская заметка
		/// </summary>
		public string UserTag { get; set; }

	}
}
