﻿using System;

namespace Market.Common.WebSockets
{
	/// <summary>
	/// Интерфейс описывает сервис получения информации действии и его аргументе
	/// </summary>
	public interface IWebSocketCommandService
	{
		/// <summary>
		/// Получить информацию о действии и его аргументе
		/// </summary>
		/// <param name="buffer">Буфер байтового массива полученного по веб-соккету</param>
		/// <param name="count">Число заполненых байтов</param>
		/// <returns>Информация о действии и его аргументе</returns>
		WebSocketCommand GetAction(ArraySegment<byte> buffer, int count);
	}
}
