﻿using Market.Common.Queries;
using System;
using System.Collections.Generic;

namespace Market.Common.Brokers
{
	/// <summary>
	/// Интерфейс описывает брокер запросов
	/// </summary>
	public interface IQueryBroker
	{
		/// <summary>
		/// Регистрация обработчика типа запроса
		/// </summary>
		/// <typeparam name="T">Тип необходимого запроса</typeparam>
		/// <param name="args">Аргументы запроса</param>
		void Register<T>() where T : IQuery;

		/// <summary>
		/// Выполнение запроса
		/// </summary>
		/// <typeparam name="T">Тип запроса</typeparam>
		/// <param name="args">Аргументы запроса</param>
		void Request<T>(IQueryArgs args);

		/// <summary>
		/// Выполнение запроса
		/// </summary>
		/// <param name="webApyQueryName">имя запроса для webApi</param>
		/// <param name="args">Аргументы запроса</param>
		void Request(string webApyQueryName, IQueryArgs args);

		/// <summary>
		/// Получить список всех зарегистрированных обработчиков запросов и их моделей
		/// Tuple: item1 - название запроса, item2 - обработчик запроса, item3 - тип модели фильтра для запроса
		/// </summary>
		/// <returns>Cписок всех зарегистрированных обработчиков запросов и их моделей</returns>
		List<Tuple<string, Type, Type>> GetRegisteredWebApiQueryTypes();
	}
}
