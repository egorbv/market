﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Common.Brokers
{
	public interface IMessageQueueBroker
	{
		Publisher CreatePulisher(QueueType queueType);
		void RegisterConsumer(QueueType queueType, Consumer consumer);
	}
}
