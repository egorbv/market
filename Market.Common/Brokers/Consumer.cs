﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Common.Brokers
{
	public abstract class Consumer
	{
		IConnection _connection;
		IModel _channel;
		string _queueName;


		public Consumer()
		{
		}

		public void Init(IConnection connection, IModel channel, string queueName)
		{
			_connection = connection;
			_channel = channel;
			_queueName = queueName;

			var consumer = new EventingBasicConsumer(channel);
			consumer.Received += Recived;
			channel.BasicConsume(_queueName, true, _queueName, consumer);

		}

		public abstract void Recived(object sender, BasicDeliverEventArgs e);

		private void _received(object sender, BasicDeliverEventArgs e)
		{
			Console.WriteLine(_queueName);
			var mess = Encoding.UTF8.GetString(e.Body.ToArray());
			//Console.WriteLine(s);
			//if (e.RoutingKey == "status")
			//{
			//	var msg = Encoding.UTF8.GetBytes("alive");
			//	_channel.BasicPublish("orderbook-exchange", "send-status", true, null, msg);
			//}
		}
	}
}
