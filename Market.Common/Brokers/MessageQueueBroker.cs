﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Common.Brokers
{
	public class MessageQueueBroker : IMessageQueueBroker
	{
		ConnectionFactory _connectionFactory;
		const string ORDERBOOK_EXCHANGE = "orderbook-exchange";
		const string ORDERBOOK_QUEUE = "orderbook-listener";
		const string ORDERTRADE_EXCHANGE = "ordertrade-exchange";
		const string ORDERTRADE_QUEUE = "ordertrade-queue";

		

		int _orderbookCurrentIndex = 0;
		QueueNameType _queueNameType;

		public MessageQueueBroker()
		{
			_connectionFactory = new ConnectionFactory() { HostName = "localhost" };
			_queueNameType = QueueNameType.SerualNumber;
		}

		public Publisher CreatePulisher(QueueType queueType)
		{
			var connection = _connectionFactory.CreateConnection();
			var exchangeName = _getExchangeName(queueType);
			var channel = connection.CreateModel();
			channel.ExchangeDeclare(exchangeName, ExchangeType.Fanout);

			var publisher = new Publisher(connection, channel, exchangeName);
			return publisher; 
		}

		public void RegisterConsumer(QueueType queueType, Consumer consumer)
		{
			var connection = _connectionFactory.CreateConnection();
			var channel = connection.CreateModel();
			var exchangeName = _getExchangeName(queueType);
			var queueName = _getOrderBookQueueName();

			channel.ExchangeDeclare(exchangeName, ExchangeType.Fanout);
			channel.QueueDeclare(queueName, false, false, true, null);
			channel.QueueBind(queueName, exchangeName, "", null);

			consumer.Init(connection, channel, queueName);
		}

		string _getExchangeName(QueueType queueType)
		{
			switch(queueType)
			{
				case QueueType.OrderBook:
					return ORDERBOOK_EXCHANGE;
				case QueueType.OrderTrade:
					return ORDERTRADE_EXCHANGE;
				default:
					throw new Exception("Данный тип Exchange не поддерживается");
			}
		}

		string _getOrderBookQueueName()
		{
			if (_queueNameType == QueueNameType.MachineName)
			{
				return Environment.MachineName;
			}
			else
			{
				_orderbookCurrentIndex ++;
				return ORDERBOOK_QUEUE + "-" + _orderbookCurrentIndex;
			}
		}
	}


	public enum QueueType
	{
		OrderBook,
		OrderTrade,
		
	}

	/// <summary>
	/// Перчисление описываещие тип наименования очередей
	/// </summary>
	public enum QueueNameType
	{
		/// <summary>
		/// К названию очереди добавляется название машины
		/// </summary>
		MachineName = 1,

		/// <summary>
		/// К названию очереди добавляетсяя порядковый номер
		/// </summary>
		SerualNumber = 2
	}

}
