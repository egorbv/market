﻿using Market.Common.Actions;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Market.Common.Data;
using System.Linq;

namespace Market.Common.Brokers
{
	/// <summary>
	/// Брокер действий
	/// </summary>
	public class ActionBroker : IActionBroker
	{
		#region Fields
		/// <summary>
		/// Словарь зарегистрированных действий
		/// </summary>
		Dictionary<Type, IAction> _actions;

		/// <summary>
		/// Список обработчиков дейсьвий для web api
		/// </summary>
		Dictionary<string, IAction> _webApiActions;

		/// <summary>
		/// Экземпляр провайдера сервисов
		/// </summary>
		IServiceProvider _serviceProvider;

		/// <summary>
		/// Объект синхронизации
		/// </summary>
		object _sync;
		#endregion

		#region constructor
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="serviceProvider">Экземпляр провайдера сервисов</param>
		public ActionBroker(IServiceProvider serviceProvider)
		{
			_actions = new Dictionary<Type, IAction>();
			_webApiActions = new Dictionary<string, IAction>();
			_sync = new object();
			_serviceProvider = serviceProvider;
		}
		#endregion

		#region Execute
		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <typeparam name="T">Тип выполняемого действия</typeparam>
		/// <param name="args">Аргументы действия</param>
		public void Execute<T>(IActionArgs args)
		{
			var type = typeof(T);
			if (_actions.ContainsKey(type))
			{
				var action = _actions[type];
				args.ConnectionType = new StoreConnectionType();
				action.Execute(args);
			}
		}


		/// <summary>
		/// Выполнить обработчик действия
		/// </summary>
		/// <param name="webApyQueryName">Название действия для web api</param>
		/// <param name="args">Аргументы действия</param>
		public void Execute(string webApyQueryName, IActionArgs args)
		{
			if (_webApiActions.ContainsKey(webApyQueryName))
			{
				var action = _webApiActions[webApyQueryName];
				args.ConnectionType = new StoreConnectionType();
				action.Execute(args);
			}
		}
		#endregion

		#region Register
		/// <summary>
		/// Зарегистрировать обаботчик для действия
		/// </summary>
		/// <typeparam name="T">Тип действия</typeparam>
		public void Register<T>() where T : IAction
		{
			lock (_sync)
			{
				if (_actions.ContainsKey(typeof(T)))
				{
					throw new Exception($"Action {typeof(T)} allready trgistered");
				}
				var actionHandler = _serviceProvider.GetRequiredService<T>();
				_actions.Add(typeof(T), actionHandler);

				var actionWebApiAttribute = typeof(T).GetCustomAttributes(false)
					.SingleOrDefault(x => x.GetType() == typeof(ActionWebApiAttribute)) as ActionWebApiAttribute;

				if (actionWebApiAttribute != null)
				{
					if(_webApiActions.ContainsKey(actionWebApiAttribute.Name))
					{
						throw new Exception($"Web Api Action {typeof(T)} allready registered");
					}
					_webApiActions.Add(actionWebApiAttribute.Name, actionHandler);
				}
			}
		}
		#endregion

		#region GetRegisteredActionTypes
		/// <summary>
		/// Получить список всех зарегистрированных обработчиков действий и их моделей
		/// Tuple: item1 - название действия, item2 - обработчик действия, item3 - тип модели для запроса действия
		/// </summary>
		/// <returns>Cписок всех зарегистрированных обработчиков действий и их моделей</returns>
		public List<Tuple<string, Type, Type>> GetRegisteredWebApiActionTypes()
		{
			var count = _actions.Count;
			var registeredActions = new List<Tuple<string, Type, Type>>();
			foreach(var item in _webApiActions)
			{
				var actionModel = item.Value.GetType().GetCustomAttributes(true)
					.SingleOrDefault(x => x.GetType() == typeof(ActionModelAttribute)) as ActionModelAttribute;

				if (actionModel != null)
				{
					registeredActions.Add(new Tuple<string, Type, Type>(item.Key, item.Value.GetType(), actionModel.Type));
				}
				else
				{
					registeredActions.Add(new Tuple<string, Type, Type>(item.Key, item.Value.GetType(), null));
				}
			}
			return registeredActions;
		}
		#endregion
	}
}
