﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Market.Common.Brokers
{
	public class Publisher
	{
		IConnection _connection;
		IModel _channel;
		string _exchangeName;

		public Publisher(IConnection connection, IModel channel, string exchangeName)
		{
			_connection = connection;
			_channel = channel;
			_exchangeName = exchangeName;
		}


		public void Publish(string message)
		{

			var msg = Encoding.UTF8.GetBytes(message);
			_channel.BasicPublish(_exchangeName, "update", true, null, msg);
			
		}

		public void Publish(object o)
		{
			var message = JsonSerializer.Serialize(o);
			var msg = Encoding.UTF8.GetBytes(message);
			_channel.BasicPublish(_exchangeName, "update", true, null, msg);

		}
	}
}
