﻿using System;
using System.Data;

namespace Market.Common.Data
{
	public static class DataReaderHelper
	{
		public static T GetValue<T>(this IDataReader reader, string fieldName)
		{
			var value = reader[fieldName];
			if (value is DBNull)
			{
				return default(T);
			}
			return (T)value;
		}

	}
}
