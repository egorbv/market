﻿namespace Market.Common.Data
{
	/// <summary>
	/// Интерфейс описывает тип подключения к источнику данных
	/// </summary>
	public interface IStoreConnectionType
	{
		/// <summary>
		/// Получение экземпляра класса подключения к источнику данных
		/// </summary>
		/// <returns></returns>
		IStoreConnection CreateConnection();
	}
}
