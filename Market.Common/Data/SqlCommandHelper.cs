﻿using System.Data.SqlClient;
using System;
using System.Data;

namespace Market.Common.Data
{
	public static class SqlCommandHelper
	{
		public static void AddParameter(this IDbCommand command, string parameterName, SqlDbType type, object value)
		{
			command.Parameters.Add(new SqlParameter(parameterName, value ?? DBNull.Value) { SqlDbType = type });
		}
	}
}
