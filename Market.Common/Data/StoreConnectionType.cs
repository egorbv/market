﻿using System.Data.SqlClient;

namespace Market.Common.Data
{
	public class StoreConnectionType : IStoreConnectionType
	{
		IStoreConnection _storeConnection;
		SqlConnection _connection;
		static string _defaultConnectionString;

		public StoreConnectionType()
		{
			
			_connection = new SqlConnection(_defaultConnectionString);
		}

		public StoreConnectionType(string connectionString)
		{
			_connection = new SqlConnection(connectionString);
		}

		public static void SetDefaultConnectionString(string connectionString)
		{
			_defaultConnectionString = connectionString;
		}

		public IStoreConnection CreateConnection()
		{
			if (_storeConnection == null)
			{
				_storeConnection = new StoreConnection(null, _connection);
				return _storeConnection;
			}
			else
			{
				return _storeConnection.CreateConnection();
			}
		}
	}
}
