﻿namespace Market.Common.Queries
{
	/// <summary>
	/// Интерфейс описывает базовый запрос
	/// </summary>
	public interface IQuery
	{
		/// <summary>
		/// Запросить данные
		/// </summary>
		/// <param name="args">Аргумент запроса</param>
		void Request(IQueryArgs args);
	}
}
