﻿using System;

namespace Market.Common.Queries
{
	/// <summary>
	/// Атрибут описывает модель фильтра для запроса
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class QueryFilterModelAttribute: Attribute
	{
		/// <summary>
		/// Тип модели фильтра для запроса
		/// </summary>
		public Type Type { get; set; }
	}
}
