﻿using System;

namespace Market.Common.Queries
{
	/// <summary>
	/// Атрибут указывает имя для запроса по web api
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface)]
	public class QueryWebApiAttribute : Attribute
	{
		/// <summary>
		/// Название запроса для web api
		/// </summary>
		public string Name { get; set; }
	}
}
