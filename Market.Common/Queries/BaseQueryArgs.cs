﻿using Market.Common.Data;

namespace Market.Common.Queries
{
	/// <summary>
	/// Базовый класс для аргумента запроса
	/// </summary>
	public class BaseQueryArgs : IQueryArgs
	{
		/// <summary>
		/// Идентификатор пользователя
		/// </summary>
		public int UserID { get; set; }

		/// <summary>
		/// Тип подсоеденения к БД
		/// </summary>
		public IStoreConnectionType ConnectionType { get; set; }

		/// <summary>
		/// Фильтр для запроса если он есть
		/// </summary>
		public object FilterModel { get; set; }

		/// <summary>
		/// Результат выполнения запроса
		/// </summary>
		public object ReturnModel { get; set; }

		/// <summary>
		/// Пользовательская метка для запроса
		/// </summary>
		public string UserTag { get; set; }
	}
}
