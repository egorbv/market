﻿using Market.Common.Data;

namespace Market.Common.Queries
{
	/// <summary>
	/// Интерфейс описывает базовый аргумент для запроса
	/// </summary>
	public interface IQueryArgs
	{
		/// <summary>
		/// Идентификатор пользователя
		/// </summary>
		int UserID { get; set; }

		/// <summary>
		/// Тип подсоеденения к БД
		/// </summary>
		IStoreConnectionType ConnectionType { get; set; }

		/// <summary>
		/// Фильтр для запроса если он есть
		/// </summary>
		object FilterModel { get; set; }

		/// <summary>
		/// Результат выполнения запроса
		/// </summary>
		object ReturnModel { get; set; }

		/// <summary>
		/// Пользовательская метка для запроса
		/// </summary>
		string UserTag { get; set; }
	}
}
