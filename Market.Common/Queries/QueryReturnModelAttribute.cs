﻿using System;

namespace Market.Common.Queries
{
	/// <summary>
	/// Атрибут описывает модель которую возвращает запрос
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class QueryReturnModelAttribute : Attribute
	{
		/// <summary>
		/// Тип модели возвращаемую запросом
		/// </summary>
		public Type Type { get; set; }
	}
}
