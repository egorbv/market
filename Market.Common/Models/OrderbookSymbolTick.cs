﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Common.Models
{
	public class OrderbookSymbolTick
	{
		public string Symbol { get; set; }
		public decimal Bid { get; set; }
		public decimal Ask { get; set; }

		public List<Tick> Bids { get; set; }

		public List<Tick> Asks { get; set; }

		public DateTime Time { get; set; }

		public OrderbookSymbolTick()
		{
			Bids = new List<Tick>();
			Asks = new List<Tick>();
		}
	}

	public class Tick
	{
		public decimal P { get; set; }
		public double V { get; set; }
	}
}
