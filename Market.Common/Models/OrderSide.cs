﻿namespace Market.Common.Models
{
	/// <summary>
	/// Перичесление описывает сторону ордера
	/// </summary>
	public enum OrderSide
	{
		/// <summary>
		/// Покупка
		/// </summary>
		Buy = 1,

		/// <summary>
		/// Продажа
		/// </summary>
		Sell = 2,
	}
}
