﻿namespace Market.Common.Models
{
	/// <summary>
	/// Интерфейс описывает тип ордера
	/// </summary>
	public enum OrderType
	{
		/// <summary>
		/// Маркет ордер
		/// </summary>
		Market = 1,

		/// <summary>
		/// Лимитный ордер
		/// </summary>
		Limit = 2,

		/// <summary>
		/// Стоп лимитный ордер
		/// </summary>
		StopLimit = 4
	}
}
