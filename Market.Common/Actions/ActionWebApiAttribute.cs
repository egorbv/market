﻿using System;

namespace Market.Common.Actions
{
	/// <summary>
	/// Атрибут указывает название для действия web api
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface)]
	public class ActionWebApiAttribute : Attribute
	{
		/// <summary>
		/// Название действия web api
		/// </summary>
		public string Name { get; set; }
	}
}
