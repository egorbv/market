﻿using System;

namespace Market.Common.Actions
{
	/// <summary>
	/// Обязательный атрибут для действия
	/// </summary>
	public class ActionModelAttribute : Attribute
	{
		/// <summary>
		/// Обрабатываемый действием тип модели 
		/// </summary>
		public Type Type{ get; set; }
	}
}
