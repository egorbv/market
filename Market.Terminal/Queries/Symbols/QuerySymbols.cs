﻿using Market.Common.Data;
using Market.Common.Queries;
using Market.Terminal.Models.Symbols;
using System.Collections.Generic;

namespace Market.Terminal.Queries.Symbols
{
	/// <summary>
	/// Обработчик запроса на получения списка символов
	/// </summary>
	[QueryReturnModel(Type = typeof(List<Symbol>))]
	public class QuerySymbols : IQuerySymbols
	{
		/// <summary>
		/// Выполнить запрос
		/// </summary>
		/// <param name="args">Аргумент запроса</param>
		public void Request(IQueryArgs args)
		{
			var symbols = new List<Symbol>();

			using(var connection = args.ConnectionType.CreateConnection())
			{
				using(var command = connection.NextCommand("terminal.QuerySymbols"))
				{
					using(var reader = command.ExecuteReader())
					{
						while(reader.Read())
						{
							symbols.Add(new Symbol
							{
								ID = reader.GetValue<int>("ID"),
								Name = reader.GetValue<string>("Name"),
								BaseCurrencyID = reader.GetValue<int>("BaseCurrencyID"),
								BaseCurrency = reader.GetValue<string>("BaseCurrency"),
								QuotedCurrencyID = reader.GetValue<int>("QuotedCurrencyID"),
								QuotedCurrency = reader.GetValue<string>("QuotedCurrency"),
								BaseCurrencyCommission = reader.GetValue<decimal>("BaseCurrencyCommission"),
								QuotedCurrencyCommission = reader.GetValue<decimal>("QuotedCurrencyCommission")
							});
						}
					}
				}
			}
			args.ReturnModel = symbols;
		}
	}
}
