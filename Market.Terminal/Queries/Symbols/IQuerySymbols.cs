﻿using Market.Common.Queries;

namespace Market.Terminal.Queries.Symbols
{
	/// <summary>
	/// Интерфейс описывает запрос на получения списка символов
	/// </summary>
	[QueryWebApi(Name = "query-symbols")]
	public interface IQuerySymbols : IQuery
	{
	}
}
