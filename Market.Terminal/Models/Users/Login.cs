﻿namespace Market.Terminal.Models.Users
{
	/// <summary>
	/// Модель для авторизации пользователя
	/// </summary>
	public class Login
	{
		/// <summary>
		/// EMail пользоватиеля
		/// </summary>
		public string EMail { get; set; }

		/// <summary>
		/// Пароль пользователя
		/// </summary>
		public string Password { get; set; }
	}
}
