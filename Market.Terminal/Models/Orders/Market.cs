﻿using Market.Common.Models;

namespace Market.Terminal.Models.Orders
{
	/// <summary>
	/// Модель содержит данные для слздания маркет оордера
	/// </summary>
	public class Market
	{
		/// <summary>
		/// Количестиво базовой валюты
		/// </summary>
		public decimal Size { get; set; }

		/// <summary>
		/// Текуща цена в OrderBook
		/// </summary>
		public decimal CurrentMarketPrice { get; set; }

		/// <summary>
		/// Сторона ордера (Buy \ Sell)
		/// </summary>
		public OrderSide OrderSide { get; set; }
	}
}
