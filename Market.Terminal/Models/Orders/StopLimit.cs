﻿using Market.Common.Models;

namespace Market.Terminal.Models.Orders
{
	/// <summary>
	/// Модель для создания стоп лимт ордера
	/// </summary>
	public class StopLimit
	{
		/// <summary>
		/// Количестиво базовой валюты
		/// </summary>
		public decimal Size { get; set; }

		/// <summary>
		/// Цена по которой готовы купить \ продать
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// Цена при которой ордер должен встать в OrderBook
		/// </summary>
		public decimal StopPrice { get; set; }

		/// <summary>
		/// Сторона ордера (Buy \ Sell)
		/// </summary>
		public OrderSide OrderSide { get; set; }
	}
}
