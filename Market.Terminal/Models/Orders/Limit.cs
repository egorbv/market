﻿using Market.Common.Models;

namespace Market.Terminal.Models.Orders
{
	/// <summary>
	/// Модель содержит данные для создания лимитного ордероа
	/// </summary>
	public class Limit
	{
		/// <summary>
		/// Количестиво базовой валюты
		/// </summary>
		public decimal Size { get; set; }

		/// <summary>
		/// Цена по которой готовы купить \ продать
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// Коммисия
		/// </summary>
		public decimal Fee { get; set; }

		/// <summary>
		/// Сторона ордера (Buy \ Sell)
		/// </summary>
		public OrderSide OrderSide { get; set; }

		/// <summary>
		/// Идентификатор символа
		/// </summary>
		public int SymbolID { get; set; }
	}
}
