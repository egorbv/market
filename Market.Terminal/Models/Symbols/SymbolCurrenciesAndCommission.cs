﻿namespace Market.Terminal.Models.Symbols
{
	/// <summary>
	/// Модель
	/// </summary>
	public class SymbolCurrenciesAndCommission
	{
		/// <summary>
		/// Идентификатор базовой валюты символа
		/// </summary>
		public int BaseCurrencyID;

		/// <summary>
		/// Идентификатор квотируемой валюты символа
		/// </summary>
		public int QuotedCurrencyID;


		/// <summary>
		/// Комисися за покупку или продажу
		/// </summary>
		public decimal Fee;
	}
}
