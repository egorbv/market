﻿namespace Market.Terminal.Models.Symbols
{
	/// <summary>
	/// Модель символа
	/// </summary>
	public class Symbol
	{
		/// <summary>
		/// Идентификатор символа
		/// </summary>
		public int ID;

		/// <summary>
		/// Базовая валюта
		/// </summary>
		public int BaseCurrencyID;

		/// <summary>
		/// Квотируемая валюта
		/// </summary>
		public int QuotedCurrencyID;

		public string Name { get; set; }

		/// <summary>
		/// Коммисия за продажу базовой валюты
		/// </summary>
		public decimal BaseCurrencyCommission { get; set; }

		/// <summary>
		/// Комисия за покупку базовой валюты
		/// </summary>
		public decimal QuotedCurrencyCommission { get; set; }

		public string BaseCurrency { get; set; }

		public string QuotedCurrency { get; set; }
	}
}
