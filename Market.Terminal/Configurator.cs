﻿using Market.Common.Brokers;
using Microsoft.Extensions.DependencyInjection;
using Market.Terminal.Queries.Symbols;
using Market.Terminal.Services;
using Market.Terminal.Actions.Orders.CreateLimit;
using Market.Terminal.Actions.Users;

namespace Market.Terminal
{
	/// <summary>
	/// Конфигуратор сервисов и обработчиков терминала
	/// </summary>
	public static class Configurator
	{
		/// <summary>
		/// Регистрация сервисов терминала
		/// </summary>
		/// <param name="serviceCollection">Экземпляр коллекции внедренных сервисов</param>
		public static void RegisterServices(IServiceCollection serviceCollection)
		{
			serviceCollection.AddSingleton<ISymbolService, SymbolService>();

			serviceCollection.AddSingleton<ICreateLimitAction, CreateLimitAction>();
			serviceCollection.AddSingleton<IAuthorize, Authorize>();

			serviceCollection.AddSingleton<IQuerySymbols, QuerySymbols>();
		}

		/// <summary>
		/// Регистрация обработчиков терминала
		/// </summary>
		/// <param name="actionBroker">Экземпляр брокера экшенов</param>
		/// <param name="queryBroker">Экземпляр брокера запросов</param>
		public static void RegisterHandlers(IActionBroker actionBroker, IQueryBroker queryBroker)
		{
			actionBroker.Register<ICreateLimitAction>();
			actionBroker.Register<IAuthorize>();

			queryBroker.Register<IQuerySymbols>();
		}
	}
}
