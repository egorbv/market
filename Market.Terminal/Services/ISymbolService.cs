﻿using Market.Common.Models;
using Market.Terminal.Models.Symbols;

namespace Market.Terminal.Services
{
	public interface ISymbolService
	{
		SymbolCurrenciesAndCommission GetSymbolInfo(int symbolID, OrderSide orderSide , decimal amount);
	}
}
