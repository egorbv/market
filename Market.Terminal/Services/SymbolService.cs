﻿using Market.Common.Brokers;
using Market.Common.Models;
using Market.Common.Queries;
using Market.Terminal.Models.Symbols;
using Market.Terminal.Queries.Symbols;
using System.Collections.Generic;


namespace Market.Terminal.Services
{
	public class SymbolService : ISymbolService
	{
		IQueryBroker _queryBroker;
		Dictionary<int, Symbol> _symbols;
		object _sync;

		public SymbolService(IQueryBroker queryBroker)
		{
			_symbols = new Dictionary<int, Symbol>();
			_sync = new object();
			_queryBroker = queryBroker;
		}

		public SymbolCurrenciesAndCommission GetSymbolInfo(int symbolID, OrderSide orderSide, decimal size)
		{
			if(!_symbols.ContainsKey(symbolID))
			{
				lock(_sync)
				{
					if(!_symbols.ContainsKey(symbolID))
					{
						_executeQuerySymbols();
					}
				}
			}
			var symbol = _symbols[symbolID];
			var symbolInfo = new SymbolCurrenciesAndCommission
			{
				BaseCurrencyID = symbol.BaseCurrencyID,
				QuotedCurrencyID = symbol.QuotedCurrencyID
			};

			if(orderSide == OrderSide.Buy)
			{
				symbolInfo.Fee = size * symbol.BaseCurrencyCommission;
			}
			else
			{
				symbolInfo.Fee = size * symbol.QuotedCurrencyCommission;
			}
			return symbolInfo;
		}


		void _executeQuerySymbols()
		{
			var queryArg = new BaseQueryArgs();
			_queryBroker.Request<IQuerySymbols>(queryArg);
			_symbols.Clear();
			var symbols = queryArg.ReturnModel as List<Symbol>;
			var symbolCount = symbols.Count;

			for(var i=0; i < symbolCount; i++)
			{
				var symbol = symbols[i];
				_symbols.Add(symbol.ID, symbol);
			}
		}
	}
}
