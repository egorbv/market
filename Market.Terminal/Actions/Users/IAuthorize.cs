﻿using Market.Common.Actions;

namespace Market.Terminal.Actions.Users
{
	[ActionWebApi(Name = "user-authorize")]
	public interface IAuthorize : IAction
	{
	}
}
