﻿using Market.Common.Actions;
using Market.Common.Data;
using Market.Terminal.Models.Users;
using System.Data;
using System.Text.Json.Serialization;

namespace Market.Terminal.Actions.Users
{
	[ActionModel(Type = typeof(Login))]
	public class Authorize : IAuthorize
	{
		public void Execute(IActionArgs args)
		{
			var model = args.Model as Login;
			using(var connection = args.ConnectionType.CreateConnection())
			{
				using (var command = connection.NextCommand("terminal.UserAuthorizate"))
				{
					command.AddParameter("EMail", SqlDbType.VarChar, model.EMail);
					command.AddParameter("Password", SqlDbType.VarChar, model.Password);
					using(var reader = command.ExecuteReader())
					{
						if(!reader.Read())
						{
							args.RetunedModel = AuthorizeRezult.WrongEmailOrLogin;
							return;
						}

						var userID = reader.GetValue<int>("ID");
						args.UserID = userID;
						args.RetunedModel = AuthorizeRezult.Successfull;
					}
				}
			}
		}
	}

	
	public enum AuthorizeRezult
	{
		Successfull = 1,
		WrongEmailOrLogin = 2
	}
}
