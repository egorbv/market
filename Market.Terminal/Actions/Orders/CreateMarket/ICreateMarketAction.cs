﻿using Market.Common.Actions;

namespace Market.Terminal.Actions.Orders.CreateMarket
{
	/// <summary>
	/// Интерфейс опсывает экшен создания маркет ордера
	/// </summary>
	[ActionWebApi(Name = "order-create-market")]
	public interface ICreateMarketAction : IAction
	{
	}
}
