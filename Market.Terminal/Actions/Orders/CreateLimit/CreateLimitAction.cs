﻿using System;
using System.Data;
using Market.Common.Actions;
using Market.Common.Data;
using Market.Common.Models;
using Market.Terminal.Models.Orders;
using Market.Terminal.Services;

namespace Market.Terminal.Actions.Orders.CreateLimit
{
	/// <summary>
	/// Обработчик экшена ICreateLimitAction
	/// </summary>
	[ActionModel(Type = typeof(Limit))]
	public class CreateLimitAction : ICreateLimitAction
	{
		/// <summary>
		/// Экземпляр сервиса символов
		/// </summary>
		ISymbolService _symbolService;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="symbolService">Экземпляр сервиса символов</param>
		public CreateLimitAction(ISymbolService symbolService)
		{
			_symbolService = symbolService;
		}

		/// <summary>
		/// Исполнение экшена
		/// </summary>
		/// <param name="args">Аргумент экшена</param>
		public void Execute(IActionArgs args)
		{
			var orderArgs = args.Model as Limit;
			var symbolInfo = _symbolService.GetSymbolInfo(orderArgs.SymbolID, orderArgs.OrderSide, orderArgs.Size);

			long orderID = 0;
			using (var connection = args.ConnectionType.CreateConnection())
			{
				using (var command = connection.NextCommand("terminal.OrderCreateLimit"))
				{
					command.AddParameter("Size", SqlDbType.Decimal, orderArgs.Size);
					command.AddParameter("Price", SqlDbType.Decimal, orderArgs.Price);
					command.AddParameter("Fee", SqlDbType.Decimal, orderArgs.Fee);
					command.AddParameter("SymbolID", SqlDbType.Int, orderArgs.SymbolID);
					command.AddParameter("OrderSideID", SqlDbType.Int, orderArgs.OrderSide);
					command.AddParameter("OrderTypeID", SqlDbType.Int, OrderType.Limit);
					orderID = Convert.ToInt64(command.ExecuteScalar());
				}
			}

			//_messageBroker.PushNotificationAsync(new LimitCreatedNotfication() { Argument = orderID });
		}
	}
}
