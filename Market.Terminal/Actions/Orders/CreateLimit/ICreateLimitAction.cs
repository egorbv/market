﻿using Market.Common.Actions;

namespace Market.Terminal.Actions.Orders.CreateLimit
{
	/// <summary>
	/// Интерфейс опсывает экшен создания лимитного ордера
	/// </summary>
	[ActionWebApi(Name = "order-create-limit")]
	public interface ICreateLimitAction : IAction
	{
	}
}
