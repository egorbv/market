﻿using Market.Common.Actions;

namespace Market.Terminal.Actions.Orders.CreateStopLimit
{
	/// <summary>
	/// Интерфейс опсывает экшен создания стоп лимитного ордера
	/// </summary>
	[ActionWebApi(Name = "order-create-stoplimit")]
	public interface ICreateStopLimitAction : IAction
	{
	}
}
