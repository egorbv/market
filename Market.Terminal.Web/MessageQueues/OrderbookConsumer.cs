﻿using Market.Common.Brokers;
using Market.Common.Models;
using Market.Terminal.Services;
using Market.Terminal.Web.WebSockets;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Market.Terminal.Web.MessageQueues
{
	public class OrderbookConsumer : Consumer, IOrderbookConsumer
	{
		ISymbolService _symbolService;
		IWebSocketServer _webScoketServer;

		public OrderbookConsumer(IWebSocketServer webScoketServer, ISymbolService symbolService)
		{
			_symbolService = symbolService;
			_webScoketServer = webScoketServer;
		}

		public override void Recived(object sender, BasicDeliverEventArgs e)
		{
			var messessage = Encoding.UTF8.GetString(e.Body.ToArray());
			var orderbookSymbolTick = JsonSerializer.Deserialize<OrderbookSymbolTick>(messessage);
			_webScoketServer.UpdateOrderBook(messessage);
		}
	}
}
