using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Market.Common.Brokers;
using Market.Common.Data;
using Market.Common.WebSockets;
using Market.Terminal.Web.MessageQueues;
using Market.Terminal.Web.WebSockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Market.Terminal.Web
{
	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IWebSocketServer, WebSocketServer>();
			services.AddSingleton<IWebSocketCommandService, WebSocketCommandService>();
			services.AddSingleton<IQueryBroker, QueryBroker>();
			services.AddSingleton<IActionBroker, ActionBroker>();
			services.AddSingleton<IOrderbookConsumer, OrderbookConsumer>();
			services.AddSingleton<IMessageQueueBroker, MessageQueueBroker>();

			Market.Terminal.Configurator.RegisterServices(services);
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{

			var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
			var serviceProvider = serviceScopeFactory.CreateScope().ServiceProvider;

			var actionBroker = serviceProvider.GetRequiredService<IActionBroker>();
			var queryBroker = serviceProvider.GetRequiredService<IQueryBroker>();
			Market.Terminal.Configurator.RegisterHandlers(actionBroker, queryBroker);

			var webSocketService = serviceProvider.GetRequiredService<IWebSocketServer>();
			var messageQueueBroker = serviceProvider.GetRequiredService<IMessageQueueBroker>();
			var orderbookConsumer = serviceProvider.GetRequiredService<IOrderbookConsumer>();
			messageQueueBroker.RegisterConsumer(QueueType.OrderBook, orderbookConsumer as Consumer);

			StoreConnectionType.SetDefaultConnectionString("Server=.;Database=Terminal;Trusted_Connection=True;ConnectRetryCount=0");

			app.UseWebSockets();
			app.Use(async (context, next) => 
			{
				if (context.WebSockets.IsWebSocketRequest)
				{
					var webSocket = await context.WebSockets.AcceptWebSocketAsync().ConfigureAwait(false);
					await webSocketService.AddWebSocket(webSocket);
				}
				else
				{
					await next();
				}
			});


			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapGet("/", async context =>
				{
					await context.Response.WriteAsync("Hello World!");
				});
			});
		}
	}
}
