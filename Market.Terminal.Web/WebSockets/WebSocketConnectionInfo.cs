﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Terminal.Web.WebSockets
{
	public class WebSocketConnectionInfo
	{
		public Guid ConnectionID;
		public int? UserID;
	}
}
