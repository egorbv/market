﻿using System.Net.WebSockets;
using System.Threading.Tasks;

namespace Market.Terminal.Web.WebSockets
{
	/// <summary>
	/// Интерфейс описывает веб-соккет сервер
	/// </summary>
	public interface IWebSocketServer
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="webSocket"></param>
		/// <returns></returns>
		Task AddWebSocket(WebSocket webSocket);

		void UpdateOrderBook(string message);
	}
}
