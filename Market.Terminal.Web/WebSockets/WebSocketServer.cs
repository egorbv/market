﻿using Market.Common.Actions;
using Market.Common.Brokers;
using Market.Common.Queries;
using Market.Common.WebSockets;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Market.Terminal.Web.WebSockets
{
	public class WebSocketServer : IWebSocketServer
	{
		IWebSocketCommandService _webSocketCommandService;
		IActionBroker _actionBroker;
		IQueryBroker _queryBroker;
		ConcurrentDictionary<WebSocket, WebSocketConnectionInfo> _connections;


		public WebSocketServer(IWebSocketCommandService webSocketCommandService, IActionBroker actionBroker, IQueryBroker queryBroker)
		{
			_connections = new ConcurrentDictionary<WebSocket, WebSocketConnectionInfo>();
			_webSocketCommandService = webSocketCommandService;
			_actionBroker = actionBroker;
			_queryBroker = queryBroker;
		}

		public async Task AddWebSocket(WebSocket webSocket)
		{
			_connections.TryAdd(webSocket, new WebSocketConnectionInfo() { ConnectionID = Guid.NewGuid() });
			var buffer = new byte[1024];
			WebSocketReceiveResult result = null;
			while(webSocket.State == WebSocketState.Open)
			{
				do
				{
					result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None).ConfigureAwait(false);
				}
				while (result.EndOfMessage);

				if (!result.CloseStatus.HasValue)
				{
					_recived(webSocket, buffer, result.Count);
				}
				else
				{
					await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
					WebSocketConnectionInfo connectionInfo;
					if(_connections.TryGetValue(webSocket, out connectionInfo))
					{
						_connections.TryRemove(webSocket, out connectionInfo);
					}
					return;
				}
			}

		}

		void _recived(WebSocket webSocket, ArraySegment<byte> buffer, int count)
		{
			WebSocketConnectionInfo connectionInfo;
			if(_connections.TryGetValue(webSocket, out connectionInfo))
			{
				var socketCommand = _webSocketCommandService.GetAction(buffer, count);

				if(connectionInfo.UserID.HasValue)
				{
					if (socketCommand.Type == WebSocketCommandType.Action)
					{
						var actionArgs = new BaseActionArgs();
						actionArgs.Model = socketCommand.Args;
						_actionBroker.Execute(socketCommand.Name, actionArgs);
						SendMessage(webSocket, socketCommand, actionArgs.Model);
					}
					else if(socketCommand.Type == WebSocketCommandType.Query)
					{
						var queryArgs = new BaseQueryArgs();
						queryArgs.FilterModel = socketCommand.Args;
						_queryBroker.Request(socketCommand.Name, queryArgs);
						SendMessage(webSocket, socketCommand, queryArgs.ReturnModel);
					}
				}
				else
				{
					_tryAuthificate(webSocket, connectionInfo, socketCommand);
				}
			}
		}

		void _tryAuthificate(WebSocket webSocket, WebSocketConnectionInfo connectionInfo, WebSocketCommand socketCommand)
		{
			var actionArgs = new BaseActionArgs();
			actionArgs.Model = socketCommand.CommandArgs;
			_actionBroker.Execute(socketCommand.Name, actionArgs);
			if (actionArgs.UserID != 0)
			{
				connectionInfo.UserID = actionArgs.UserID;
			}
			SendMessage(webSocket, socketCommand, actionArgs.RetunedModel);
		}

		public void SendMessage(WebSocket webSocket, WebSocketCommand socketCommand, object messageValue)
		{
			var sendArguments = new WebSocketSendArguments
			{
				Type = socketCommand.Type,
				Name = socketCommand.Name,
				Value = messageValue,
				UserTag = socketCommand.UserTag
			};
			var setting = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, AllowTrailingCommas = true};
			setting.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));

			var msg = JsonSerializer.Serialize(sendArguments, setting);

			var buffer = new ArraySegment<byte>(UTF8Encoding.UTF8.GetBytes(msg));
			webSocket.SendAsync(buffer, WebSocketMessageType.Text, false, CancellationToken.None);
		}

		public void UpdateOrderBook(string message)
		{
			var buffer = new ArraySegment<byte>(UTF8Encoding.UTF8.GetBytes("{ \"Type\":\"Notfication\", \"name\" :\"orderbook-update\",\"value\":" + message + "}"));
			var connections = _connections.Where(x => x.Value.UserID.HasValue).Select(x=>x.Key).ToList();
			
			///Parallel must be
			foreach(var item in connections)
			{
				item.SendAsync(buffer, WebSocketMessageType.Text, false, CancellationToken.None);
			}
		}
	}
}
