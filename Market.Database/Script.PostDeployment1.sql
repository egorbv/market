﻿INSERT INTO terminal.OrderSide(ID, Name) VALUES (1, 'Buy')
INSERT INTO terminal.OrderSide(ID, Name) VALUES (2, 'Sell')

INSERT INTO terminal.OrderType(ID, Name) VALUES (1, 'Market')
INSERT INTO terminal.OrderType(ID, Name) VALUES (2, 'Limit')
INSERT INTO terminal.OrderType(ID, Name) VALUES (4, 'Stop Limit')

SET IDENTITY_INSERT terminal.Currency ON;  
INSERT INTO terminal.Currency (ID, Name) VALUES ( 1, 'ETH') 
INSERT INTO terminal.Currency (ID, Name) VALUES ( 2, 'LTC') 
INSERT INTO terminal.Currency (ID, Name) VALUES ( 3, 'EUR') 
INSERT INTO terminal.Currency (ID, Name) VALUES ( 4, 'TST1') 
INSERT INTO terminal.Currency (ID, Name) VALUES ( 5, 'TST2') 
SET IDENTITY_INSERT terminal.Currency OFF;  




SET IDENTITY_INSERT terminal.Symbol ON;  
INSERT INTO terminal.Symbol (ID, Name, BaseCurrencyID, QuotedCurrencyID) VALUES(1, 'ETHLTC',   1, 2)
INSERT INTO terminal.Symbol (ID, Name, BaseCurrencyID, QuotedCurrencyID) VALUES(2, 'LTCEUR',   2, 3)
INSERT INTO terminal.Symbol (ID, Name, BaseCurrencyID, QuotedCurrencyID) VALUES(3, 'ETHTST1',  1, 4)	
INSERT INTO terminal.Symbol (ID, Name, BaseCurrencyID, QuotedCurrencyID) VALUES(4, 'ETHTST2',  1, 5)
INSERT INTO terminal.Symbol (ID, Name, BaseCurrencyID, QuotedCurrencyID) VALUES(5, 'TST1TST2', 4, 5)
SET IDENTITY_INSERT terminal.Symbol OFF;  

SET IDENTITY_INSERT terminal.[User] ON;
INSERT INTO terminal.[User] (ID, EMail, Password, NickName) VALUES(1, 'egorbv@mail.ru', '1234', 'Egor')
INSERT INTO terminal.[User] (ID, EMail, Password, NickName) VALUES(2, 'egorbv1@mail.ru','1234', 'Egor')
SET IDENTITY_INSERT terminal.[User] OFF;  

INSERT INTO terminal.UserBalance (UserID, CurrencyID) VALUES (1,1)

