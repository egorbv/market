﻿CREATE PROCEDURE [terminal].[QuerySymbols]
AS
BEGIN
	SELECT s.ID, s.Name, s.BaseCurrencyID, c1.Name AS BaseCurrency, 
		s.QuotedCurrencyID, c2.Name AS QuotedCurrency, s.BaseCurrencyCommission, s.QuotedCurrencyCommission 
		FROM 
			Symbol s
			INNER JOIN terminal.Currency c1 ON c1.ID = s.BaseCurrencyID
			INNER JOIN terminal.Currency c2 ON c2.ID = s.QuotedCurrencyID
END
