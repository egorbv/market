﻿CREATE PROCEDURE [terminal].[OrderCreateLimit]
	@Size  DECIMAL(24,14),
	@Price DECIMAL(24,14),
	@Fee   DECIMAL(24,14),
	@SymbolID INT,
	@OrderSideID INT,
	@OrderTypeID INT
AS
BEGIN
	INSERT INTO terminal.[Order]
			(Size, Price, Fee, SymbolID, OrderSideID, OrderTypeID)
		VALUES
			(@Size, @Price, @Fee, @SymbolID, @OrderSideID, @OrderTypeID)
	RETURN SCOPE_IDENTITY()
END
