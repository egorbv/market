﻿CREATE PROCEDURE [terminal].[UserAuthorizate]
	@EMail VARCHAR(128),
	@Password VARCHAR(128)
AS
BEGIN
DECLARE @UserID INT

	UPDATE terminal.[User]
		SET
			LastLoginDate = SYSDATETIMEOFFSET (),
			@UserID = ID
		WHERE
			EMail = @EMail
			AND Password = @Password
	
	IF @UserID IS NOT NULL
	BEGIN
		INSERT INTo terminal.UserLoginHistory(UserID) VALUES(@UserID)
		SELECT ID FROM terminal.[User] WHERE ID = @UserID
	END
END
