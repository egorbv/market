﻿CREATE TABLE [terminal].[Order] (
    [ID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [OrderTypeID]  INT NOT NULL,
    [OrderSideID]  INT              NOT NULL,
    [SymbolID]     INT              NOT NULL,
    [Size]         DECIMAL (28, 14) NOT NULL,
    [FilledAmount] DECIMAL (28, 14) NOT NULL DEFAULT 0,
    [Price]        DECIMAL (28, 14) NOT NULL,
    [Fee]          DECIMAL (28, 14) NOT NULL,
    CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO;
ALTER TABLE [terminal].[Order]  
    ADD  CONSTRAINT [FK_Order_OrderSide] FOREIGN KEY([OrderSideID])
    REFERENCES [terminal].[OrderSide] ([ID]);

GO;
ALTER TABLE [terminal].[Order]  
    ADD  CONSTRAINT [FK_Order_Symbol] FOREIGN KEY([SymbolID])
    REFERENCES [terminal].[Symbol] ([ID]);

GO;
ALTER TABLE [terminal].[Order]  
    ADD  CONSTRAINT [FK_Order_OrderType] FOREIGN KEY([OrderTypeID])
    REFERENCES [terminal].[OrderType] ([ID]);
