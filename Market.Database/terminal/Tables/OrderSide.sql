﻿CREATE TABLE [terminal].[OrderSide]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL
)
