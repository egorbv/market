﻿CREATE TABLE [terminal].[Symbol]
(
	[ID]               INT NOT NULL  IDENTITY (1, 1) , 
    [Name]             VARCHAR(64)   NOT NULL,
    [BaseCurrencyID]   INT NOT NULL, 
    [QuotedCurrencyID] INT NOT NULL, 
    [BaseCurrencyCommission]   DECIMAL NOT NULL DEFAULT(0), 
    [QuotedCurrencyCommission] DECIMAL(24,14) NOT NULL DEFAULT(0), 
    CONSTRAINT [PK_Symbol] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO;
ALTER TABLE [terminal].[Symbol]  
    ADD  CONSTRAINT [FK_Symbol_Currency1] FOREIGN KEY([BaseCurrencyID])
    REFERENCES [terminal].[Currency] ([ID]);

GO;
ALTER TABLE [terminal].[Symbol]  
    ADD  CONSTRAINT [FK_Symbol_Currency2] FOREIGN KEY([QuotedCurrencyID])
    REFERENCES [terminal].[Currency] ([ID]);
    