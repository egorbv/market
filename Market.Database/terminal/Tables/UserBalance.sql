﻿CREATE TABLE [terminal].[UserBalance]
(
	[UserID] INT NOT NULL,
	[ID] INT NOT NULL IDENTITY (1, 1),
	[CurrencyID] INT NOT NULL,
	[TradeBalance] DECIMAL(28,14) NOT NULL DEFAULT(0),
	[HoldOnOrderBalance] DECIMAL(28,14) NOT NULL DEFAULT(0),
	[TradingFeeBalance] DECIMAL(28,14) NOT NULL DEFAULT(0),
	[FundingFeeBalance] DECIMAL(28,14) NOT NULL DEFAULT(0),
    CONSTRAINT [PK_UserBalance] PRIMARY KEY CLUSTERED ([UserID],[ID] ASC)
)

GO;
ALTER TABLE [terminal].[UserBalance]  
    ADD  CONSTRAINT [FK_UserBalance_User] FOREIGN KEY([UserID])
    REFERENCES [terminal].[User] ([ID]);
GO;
ALTER TABLE [terminal].[UserBalance]  
    ADD  CONSTRAINT [FK_UserBalance_Currency] FOREIGN KEY([CurrencyID])
    REFERENCES [terminal].[Currency] ([ID]);
